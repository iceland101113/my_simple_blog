FactoryBot.define do
  factory :post do
    title { 'test post 1' }
    content { 'this is my first test post' }
    status { 'published' }
    user
  end
end
