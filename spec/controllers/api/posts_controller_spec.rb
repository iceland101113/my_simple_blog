require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do
  let!(:user) { FactoryBot.create :user }
  let!(:post) { FactoryBot.create :post, user: user }
  let!(:params) {{
    title: 'test post 2',
    content: 'this is second test post',
    status: 'published'
  }}

  describe 'Get #get_all_posts' do
    before do
      get :get_all_posts
    end
    it { expect(response).to have_http_status 200 }
    it { title = JSON.parse(response.body).dig('data',0,'title')
         expect(title).to eq 'test post 1'
       }
    it { data = JSON.parse(response.body).dig('data')
         expect(data.size).to eq 1
       }
  end
end
